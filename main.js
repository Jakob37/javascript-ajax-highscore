/**
 * Created by Jakob on 2015-03-29.
 */

var PhaserTemplate = PhaserTemplate || {};

PhaserTemplate.game = new Phaser.Game(1000, 1000, Phaser.AUTO, '');

PhaserTemplate.game.state.add('play', PhaserTemplate.play);

PhaserTemplate.game.state.start('play');
