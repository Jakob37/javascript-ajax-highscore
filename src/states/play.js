var PhaserTemplate = PhaserTemplate || {};

PhaserTemplate.play = function(){};

PhaserTemplate.play.prototype = {

    preload: function() {
        this.load.image('square', 'assets/sprites/white_square.png');
        this.load.text('highscoretext', 'highscore.txt')
    },

    create: function() {

        this.mysprite = this.game.add.sprite(100, 100, 'square');

        this.score = 10;
        this.highscore = 0;

        this.textstyle = { font: "16px Monospace", fill: "#ff0", align: "center" };

        this.scoreInfo = this.game.add.text(100, 70, '', this.textstyle);
        this.highScoreInfo = this.game.add.text(100, 120, '', this.textstyle);
        this.debugInfo = this.game.add.text(100, 170, '', this.textstyle);

        this.debugInfo.text = this.game.cache.getText('highscoretext');

        this.game.add.text(100, 200, 'Space to gain score.\nEnter for highscore attempt!', this.textstyle);
    },

    update: function() {
        this.mysprite.x += 1;

        this.updateText();

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.score += 1;
        }

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {

            if (this.score > this.highscore) {
                this.highscore = this.score;
            }

            this.score = 0;

            jQuery.get("assign_highscore.cgi");
        }
    },

    updateText: function() {
        this.scoreInfo.text = 'Current score: ' + this.score;
        this.highScoreInfo.text = 'Current highscore: ' + this.highscore;
    }
};
