#!/usr/local/bin/python

scores = dict()
with open('highscore.txt') as highscores_fh:
    for line in highscores_fh:
        line = line.rstrip()
        name, score = line.split('\t')
        scores[name] = int(score)

best_name_score = ('empty', 0)
for key in scores:
    if scores[key] > best_name_score[1]:
        best_name_score = (key, scores[key])

nm = best_name_score[0]
sc = best_name_score[1]

print("Content-Type: text/html")
print
print("<HEAD></HEAD>")
print("<BODY>")
print("Here is some CGI content!<BR>")
print("Best score: %s by: %s" % (sc, nm))
print("</BODY>")
